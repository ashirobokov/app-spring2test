package ru.ashirobokov.java.app.model;

public class BlogMessage {

    private Integer mesId;

    private String mesDate;

    private String mesTitle;

    private String blogMessage;

    private String mesRubric;

    private String mesLink;

    private String mesUrl;


    public Integer getMesId() {
        return mesId;
    }

    public void setMesId(Integer mesId) {
        this.mesId = mesId;
    }

    public String getMesDate() {
        return mesDate;
    }

    public void setMesDate(String mesDate) {
        this.mesDate = mesDate;
    }

    public String getMesTitle() {
        return mesTitle;
    }

    public void setMesTitle(String mesTitle) {
        this.mesTitle = mesTitle;
    }

    public String getBlogMessage() {
        return blogMessage;
    }

    public void setBlogMessage(String blogMessage) {
        this.blogMessage = blogMessage;
    }

    public String getMesRubric() {
        return mesRubric;
    }

    public void setMesRubric(String mesRubric) {
        this.mesRubric = mesRubric;
    }

    public String getMesLink() {
        return mesLink;
    }

    public void setMesLink(String mesLink) {
        this.mesLink = mesLink;
    }

    public String getMesUrl() {
        return mesUrl;
    }

    public void setMesUrl(String mesUrl) {
        this.mesUrl = mesUrl;
    }

    @Override
    public String toString() {
        return "BlogMessage [mesId=" + mesId + ", mesDate=" + mesDate
                + ", mesTitle=" + mesTitle + ", blogMessage=" + blogMessage
                + ", mesRubric=" + mesRubric + ", mesLink=" + mesLink + ", mesUrl="
                + mesUrl + "]";
    }

}
