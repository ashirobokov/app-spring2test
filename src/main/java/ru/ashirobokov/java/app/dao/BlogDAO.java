package ru.ashirobokov.java.app.dao;

import ru.ashirobokov.java.app.model.BlogMessage;

import java.util.List;

public interface BlogDAO {

    public void addMessage(BlogMessage message);
    public void updateMessage(BlogMessage message);
    public void deleteMessage(Integer mesId);
    public BlogMessage getMessage(Integer mesId);
    public List<BlogMessage> getMessages();
    public List<BlogMessage> getMessages(String rubric);
    public int countOfMessages(Integer mesId);

}
