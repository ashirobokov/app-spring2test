package ru.ashirobokov.java.app.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.ashirobokov.java.app.model.BlogMessage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class BlogDAOImpl implements BlogDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void addMessage(BlogMessage message) {
        this.jdbcTemplate.update("insert into blog_message(" +
                        "message_id,message_date, message_title, message, message_rubric, message_link, message_url)" +
                        "values (?, ?, ?, ?, ?, ?, ?)",
                        message.getMesId(), message.getMesDate(), message.getMesTitle(),
                        message.getBlogMessage(), message.getMesRubric(), message.getMesLink(),
                        message.getMesUrl());
    }

    @Override
    public void updateMessage(BlogMessage message) {
        this.jdbcTemplate.update(
                "update blog_message set message_date=?," +
                                            " message_title=?," +
                                            " message=?," +
                                            " message_rubric=?," +
                                            " message_link=?," +
                                            " message_url=?" +
                        " where message_id = ?",
                                            message.getMesDate(),
                                            message.getMesTitle(),
                                            message.getBlogMessage(),
                                            message.getMesRubric(),
                                            message.getMesLink(),
                                            message.getMesUrl(),
                        message.getMesId());
    }

    @Override
    public void deleteMessage(Integer mesId) {
        this.jdbcTemplate.update("DELETE FROM blog_message WHERE message_id = ?", Long.valueOf(mesId));
    }

    @Override
    public BlogMessage getMessage(Integer mesId) {
        BlogMessage message = this.jdbcTemplate.queryForObject(
                "select message_id, message_date, message_title, message," +
                        " message_rubric, message_link, message_url from blog_message where message_id = ?",
                new Object[]{mesId},
                new RowMapper<BlogMessage>() {
                    public BlogMessage mapRow(ResultSet rs, int rowNum) throws SQLException {
                        BlogMessage message = new BlogMessage();
                        message.setMesId(rs.getInt("message_id"));
                        message.setMesDate(rs.getString("message_date"));
                        message.setMesTitle(rs.getString("message_title"));
                        message.setBlogMessage(rs.getString("message"));
                        message.setMesRubric(rs.getString("message_rubric"));
                        message.setMesLink(rs.getString("message_link"));
                        message.setMesUrl(rs.getString("message_url"));

                        return message;
                    }
                });

    return message;
    }

    @Override
    public List<BlogMessage> getMessages() {
        return this.jdbcTemplate.query( "select message_id, message_date, message_title, message," +
                " message_rubric, message_link, message_url from blog_message", new BlogMessageMapper());
    }

    @Override
    public List<BlogMessage> getMessages(String rubric) {
        return this.jdbcTemplate.query( "select message_id, message_date, message_title, message," +
                " message_rubric, message_link, message_url from blog_message where message_rubric like ?",
                new Object[]{rubric},
                new BlogMessageMapper()
        );
    }

    @Override
    public int countOfMessages(Integer mesId) {
        int count = this.jdbcTemplate.queryForObject(
                "select count(*) from blog_message where message_id = ?", Integer.class, mesId);
        return count;
    }


    private static final class BlogMessageMapper implements RowMapper<BlogMessage> {

        public BlogMessage mapRow(ResultSet rs, int rowNum) throws SQLException {
            BlogMessage message = new BlogMessage();
            message.setMesId(rs.getInt("message_id"));
            message.setMesDate(rs.getString("message_date"));
            message.setMesTitle(rs.getString("message_title"));
            message.setBlogMessage(rs.getString("message"));
            message.setMesRubric(rs.getString("message_rubric"));
            message.setMesLink(rs.getString("message_link"));
            message.setMesUrl(rs.getString("message_url"));

            return message;
        }

    }

}
